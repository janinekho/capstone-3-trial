import {Doughnut} from 'react-chartjs-2';
import React, {useEffect, useState} from 'react';
import { Col, Row} from 'react-bootstrap';
import {colorRandomizer} from '../helpers/colorRandomizer';


export default function doughnutChart({amount,label}){
	

	/*setBgColors(data.map(() => `#${colorRandomizer()}`))*/
	
	/*useEffect(()=>{
		
 		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/transaction-details`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        
        })
      .then(res => res.json())
      .then(data => {*/

       
        
        /*let totalMap = new Map();
        // remove duplicate category by summing them up
        for (var i =0; i< data.length; i++){
          let key = data[i].transactionDescription
           if(totalMap.has(key)){
             let entry = totalMap.get(key);
             entry.amount = entry.amount + data[i].amount;
             totalMap.set(key,entry);
           } else {
            totalMap.set(key,data[i]);
         }
        }

        // set values to label
        let tempIncome = [];
        let tempIncomeLabel = [];
        let tempExpense = [];
        let tempExpenseLabel = [];
        for (let value of totalMap.values()) {
          if(value.transactionType == "Income"){
              tempIncome.push(value.amount);
              tempIncomeLabel.push(value.transactionDescription) 
              console.log(value.amount)

          } else if(value.transactionType == "expense"|| value.transactionType == "Expense"){
            tempExpense.push(value.amount)
            tempExpenseLabel.push(value.transactionDescription)
          }
        }

        setIncome(tempIncome);
        setIncomeLabel(tempIncomeLabel);
        setExpense(tempExpense);
        setExpenseLabel(tempExpenseLabel);
        
        

       console.log(income);
       })
      
    },[])*/

    const data1 ={
    	datasets:[
    		{
			data: amount,
      		backgroundColor: ["green", "#01796F", "#c7ea46","#4B5320"]

    		}],
    		labels: label
  
  	};


  	/*const data2 ={
    	datasets:[
    		{
			data: expense,
      		backgroundColor: ["red", "#01796F", "#c7ea46","#4B5320"]

    		}],
    		labels: expenseLabel
  
  	};*/
    		

   

    /*const data ={
		labels: iamount,
		datasets: [{
			data:iamount,
			backgroundColor: bgColors,
			hoverBackground: bgColors
		}]
	}*/
return(

	
   <React.Fragment>
   <Doughnut data={data1}/>
   
   
   </React.Fragment>
  )
}

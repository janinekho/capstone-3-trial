import { Line } from 'react-chartjs-2';
import React, {useEffect, useState} from 'react';
import { Card} from 'react-bootstrap';
// this component will expect 2 props
// figure Array contains the monthly figure to be displayed  ion the chart
// label is the label or header to be shown at the top of the chart
export default function LineChart({balanceData, label}){
    /*const[income, setIncome] = useState([]);
    const [expense, setExpense] = useState([]);
    const [Iamount, setIAmount] = useState(0);
    const [Eamount, setEAmount] = useState(0);*/
    
    const data = {
        labels: label,
        datasets: [
           {
             label: 'Balance',
            fill: false,
            lineTension: 0.5,
            backgroundColor: 'blue',
            borderColor: 'blue',
            borderWidth: 2,
            data: balanceData,
             
             
           },


        ]

    }

/*useEffect(()=>{

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/transaction-details`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        
        })
      .then(res => res.json())
      .then(data => {
      
      for (var i =0; i<data.length; i++){
         
        if(data[i].transactionType == "Income"){
              setIAmount(data[i].transactionType)
  
          } 
      }


       

    })
  },[])

    */

    
    return(
        <React.Fragment>
        
         <Line data={data}/>
       </React.Fragment>
     

        )
}   

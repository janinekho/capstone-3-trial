import React,{useContext, useState, useEffect} from 'react';
import { Form, Button, Row, Col,Card } from 'react-bootstrap';
import Head from 'next/head';
import Router from 'next/router';
import UserContext from '../../UserContext';
import View from '../../components/View';

/*<Form onSubmit={e => registerUser(e)}>
            	<Form.Group controlId="userEmail">
           		 	<Form.Label> Email Address </Form.Label>
           		 	<Form.Control 
             			type ="email"
             			placeholder = "Enter email"
             			value= {email}
             			onChange={e => setEmail(e.target.value)}
             			required 
            	/>
            		<Form.Text className = "text-muted">
                		We'll never share your email with anyone else.
            		 </Form.Text>   
            	</Form.Group>

    <Form.Text className = "text-muted">
                        We'll never share your email with anyone else.
                     </Form.Text>  

  
    </Form>*/


export default function edit() {
     const{ user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [name, setName]= useState('');
  const[budget,setBudget]= useState('');

  console.log()
    return(

    <div>
   <View title ={'Edit'}>
    <Row className="justify-content-center">
      <Col xs md="6">
        <h3>Edit User</h3>  
        
      </Col>
    </Row>

 
     </View>  
    


      
      <Card>
        
          <Card.Body>
             <Form onSubmit={e => authenticate(e)}>
        <Form.Group>
            <Form.Label>Full Name:</Form.Label>
            <Form.Control 
                type="text" 
                placeholder={user.name}
              onChange={(e) => setName(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group>
            <Form.Label>Budget</Form.Label>
            <Form.Control 
               type="number" 
                placeholder={user.budget}
              onChange={(e) => setBudget(e.target.value)}
                required 
               
            />
           </Form.Group> 
           <button> Submit </button>
          </Form>  
          </Card.Body>
        </Card>  
    </div>
         
  )
}
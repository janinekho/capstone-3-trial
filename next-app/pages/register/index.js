import React, {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';
import Head from 'next/head';
export default function index(){
  //all the first 3 strings will contain kung ano nasa fomr
  const[email, setEmail] = useState('');
  const[name, setName] = useState('');
  const[budget, setBudget] = useState('');
  const[password1, setPassword1] = useState('');
  const[password2, setPassword2] = useState('');
  //state to determine whether submit button is enabled or not
  const[isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password1);
  console.log(password2);

   function registerUser(e) {
    e.preventDefault();

    // clear input fields
    setEmail('');
    setPassword1('');
    setPassword2('');

    alert('Thank you for registering!');
   }

   useEffect(() => {
    //validation to enable submit button when all fields are populated and both passwords match
    if((email !== '' && password1 !== '' && password2 !== '') &&(password1 === password2)){
        setIsActive(true);
    }else {
        setIsActive(false);
    }
    
   }, [ email, password1, password2 ])

    return(
        <React.Fragment>
         <Head>
         <title> Register </title>
         </Head>
            <Form onSubmit={e => registerUser(e)}>

             <div>
             <h1 controlId="register-form"> Register Here </h1>
            <Form.Group>
                    <Form.Label> Full Name </Form.Label>
                    <Form.Control 
                        type ="text"
                        value= {name}
                        onChange={e => setName(e.target.value)}
                        required 
                />
                </Form.Group>

                <Form.Group controlId="userBudget">
                    <Form.Label> Budget </Form.Label>
                    <Form.Control 
                        type ="number"
                        value= {budget}
                        onChange={e => setBudget(e.target.value)}
                        required 
                />
                </Form.Group>


                <Form.Group controlId="userEmail">
                    <Form.Label> Email Address </Form.Label>
                    <Form.Control 
                        type ="email"
                        placeholder = "Enter email"
                        value= {email}
                        onChange={e => setEmail(e.target.value)}
                        required 
                />
                    <Form.Text className = "text-muted">
                        We'll never share your email with anyone else.
                     </Form.Text>   
                </Form.Group>

                

                <Form.Group controlId="password1">
                    <Form.Label> Password </Form.Label>
                    <Form.Control 
                        type ="password"
                        placeholder = "Enter Password"
                        value = {password1}
                        onChange = {e => setPassword1(e.target.value)}
                        required 
                />  
                </Form.Group>

                <Form.Group controlId="password2">
                    <Form.Label> Verify Password </Form.Label>
                    <Form.Control 
                        type ="password"
                        placeholder = "Verify Password"
                        value = {password2}
                        onChange = {e => setPassword2(e.target.value)}
                        required 
                />                  
                </Form.Group>
                {/*conditionally render submit button based on inActive state*/}
                {isActive
                    ?
                    <Button className="bg-primary" type="submit" id="submitBtn"> Submit
                   </Button>  
                    :
                  <Button className="bg-danger" type="submit" id="submitBtn" disabled> Submit
                  </Button> 

                }
                    
                </div>
                </Form>
        </React.Fragment>
        )
}
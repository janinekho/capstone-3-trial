import React,{ useContext, useState , useEffect} from 'react';
import { Button, Card, Form } from 'react-bootstrap';


export default function Categories(){
const [type, setType] = useState('');
const [description, setDescription]= useState('');

  function submit(e){   
      fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/add-category`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',    
        },
        body: JSON.stringify({
          type: type,
          description: description
        })
        
        })
      .then(res => res.json())
      .then(data => {       
         console.log(data);
         
    	})
     }
     console.log(type)
         console.log(description)
	return(
	<React.Fragment>
		<h3> New Category </h3>
	<Card>
	<Form onSubmit={e => submit(e)}>
	<Form.Group>
         <Form.Label> Category Type: </Form.Label>
           	<Form.Control as="select" value={type} onChange= {e => setType(e.target.value)}>>
           		<option value="" > - </option>
                <option value="Income" >Income </option>
      			<option value="Expense">Expense</option>			
            </Form.Control>
     </Form.Group>
     <Form.Group>
         <Form.Label> Category Name: </Form.Label>
           	<Form.Control as="textarea"
           	value={description}
            onChange={e => setDescription(e.target.value)}>			
            </Form.Control>
     </Form.Group>
     <button> Submit </button>
     </Form>
     </Card>
    </React.Fragment>
    )

}
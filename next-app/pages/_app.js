import React, { useState, useEffect } from 'react';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from '../components/NavBar';
import { Container } from 'react-bootstrap';
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';

  function MyApp({ Component, pageProps }) {
    
    const [user, setUser] = useState({
	   email: null,
    isAdmin: null,
  })
 
 	useEffect(()=>{
  		const options = {
        headers: {Authorization : `Bearer ${ AppHelper.getAccessToken()}`}
      }
      
      fetch(`${AppHelper.API_URL}/users/details`, options)
      .then (AppHelper.toJSON)
      .then(userData => {
        if (typeof userData._id !== 'undefined'){
          setUser({
            id: userData._id,
            isAdmin: userData.isAdmin,
            firstName: userData.firstName,
            lastName: userData.lastName,
            email: userData.email

          })
         console.log(userData);

         }
       

      })



    }, [user.id])
   
 
  

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      email:null,
      isAdmin:null,
      name: null,
      budget:0
    })
  }
  return(

  	<React.Fragment>
  	   <UserProvider value={{user,setUser, unsetUser}}>
  	 <NavBar />
  	 <Container>
   		<Component {...pageProps} />
   	 </Container>
   	 </UserProvider>	
   </React.Fragment>

   )
}


export default MyApp

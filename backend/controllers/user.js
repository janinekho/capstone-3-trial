const User = require('../models/User')
const Transaction = require('../models/Transaction')
const Category = require('../models/Category')
/*const Course = require('../models/Course')*/
const {OAuth2Client} = require('google-auth-library')
const bcrypt = require("bcrypt")
const auth = require('../auth')
const clientId= '543967147292-88s4c4mv95m2ihmem08b9lqfkiasg98p.apps.googleusercontent.com'


module.exports.emailExists = (params) => {
    return User.find({email: params.email}).then(resultFromFind => {
        return resultFromFind.length > 0 ? true : false
    })
}

module.exports.register = (params) => {
    let newUser = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        mobileNo: params.mobileNo,
        password: bcrypt.hashSync(params.password, 10), //hashSync() hashes/encrypts and the number is the salt value or how many times the password is hashed
        loginType: 'email'
    })

    return newUser.save().then((user, err) => {
        return (err) ? false : true
        
    })
}



module.exports.postTransaction= (params) =>{
    let newTransaction = new Transaction({
        userId: params.userId,
        transactionDescription: params.transactionDescription,
        transactionType: params.transactionType,
        amount: params.amount,
        remarks: params.remarks
    })


    // find a user
    return User.findById(params.userId).then(user => {

        // save to the database
        return newTransaction.save().then((transaction, err) => {
            return (err) ? false : true
       }
      )
    })

  }

  module.exports.addCategory= (params) =>{
    let newCategory= new Category({
        
        type: params.type,
        description: params.description
        
    })
    
        // save to the database
        return newCategory.save().then((category, err) => {
            return (err) ? false : true
    
    })

  }


module.exports.get = (params) => {
    return Course.findById(params.categoryId).then(category => category)
}


module.exports.login = (params) => {
    return User.findOne({email: params.email}).then(resultFromFindOne => {
        if(resultFromFindOne === null){ //user doesn't exist
            return { error: 'does-not-exist' }
        }
        if (resultFromFindOne.loginType !== 'email'){
            return { error: 'login-type-error' }
        }
        const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password) //decrypts password

        

        if(isPasswordMatched){
            return {
                accessToken: auth.createAccessToken(resultFromFindOne.toObject())
            }
        } else {
            return {error: 'incorrect-password'}
        }
    })
}

module.exports.get = (params) => {
    return User.findById(params.userId).then(user => {
        // re-assign the password to undefined so it won't be displayed along with other user data
        user.password = undefined
        return user
    })
}

//
module.exports.viewTransaction = (params) => {
    /*return Transaction.find({userId: params.userId })
    .then(transaction => {
         
         Category.findOne({_Id: transaction.categoryId})
            .then( category => { 
                transaction.description =category.description;
            })

return transaction;
        
    })*/
    return Transaction.aggregate([
{ "$match": { "userId": params.userId } },
{ $lookup:
       {
         from: 'Category',
         localField: 'categoryId',
         foreignField: '_id',
         as: 'categoryDetails'
       }
     }
    ])
.then(transactions => transactions)
 
}

module.exports.transactionType = (params) => {
}


module.exports.getCategory = () => {
    return Category.find().then(categories => categories)
}

/*module.exports.enroll = (params) => {
    // find a user
    return User.findById(params.userId).then(user => {
        // add the course ID to the user
        user.enrollments.push({ courseId: params.courseId})

        // save to the database
        return user.save().then((user, err) => {
            // find a course
            return Course.findById(params.courseId).then(course => {
                // add the user ID to the course
                course.enrollees.push({ userId: params.userId })

                // save to database
                return course.save().then((course, err) => {
                    return (err) ? false : true
                })
            })
        })
    })
}*/



 module.exports.verifyGoogleTokenId= async (tokenId) =>{
    //import package in order for us to successfully communicate with Google
    const client = new OAuth2Client(clientId);
    const data = await client.verifyIdToken({
        idToken: tokenId,
        audience: clientId
    })

    console.log(data.payload);

    /*data {
        payload{
            email_verified: true
            email:'admin@gmail.com',
            given_name:'arvin',
            family_name: lacdao

        }
    } yan yung inaaccessng asa baba*/
  
    if(data.payload.email_verified === true){
        const user = await User.findOne({email: data.payload.email}).exec();
        //find any matching emails from the database
        //.exec is a mongoose method- in this case, we are mainly using it for troubleshooting purposes
        if(user !== null) {
            if(user.loginType === "google"){
                //created using google account
                return { accessToken: auth.createAccessToken(user.toObject())} ;
                //converting user to an object
            } else{
                return { error: "login-type-error"}
            } 
        } else {
            const newUser =  new User({
                //gumagawa tayo ng panibagong object. pagmay new.. new object
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                //data.payload is from the google
                email: data.payload.email,
                loginType: "google"
            })

            return newUser.save().then((user, err) =>{
                return { accessToken: auth.createAccessToken(user.toObject()) };
            })
        }

    } else {
        //objectName.error.. object
        return{ error: "google-auth-error"}
    }

 }